#!/bin/bash

# get the number of unpushed commits and remove whitespace
count=$(git log --branches --not --remotes --oneline | wc -l | tr -d '[:space:]')

# generate xml files
sfdx sgd:source:delta --to "HEAD" --from "HEAD~$count" --output ./manifest