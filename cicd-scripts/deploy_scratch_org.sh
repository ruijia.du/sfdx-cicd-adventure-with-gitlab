#!/bin/bash

assert_within_limits() {

  export local org_username=$1
  export local limit_name=$2
  echo "org_username=$org_username" >&2
  echo "limit_name=$limit_name" >&2

  local cmd="sf limits api display --target-org $org_username --json" && (echo $cmd >&2)
  local limits=$($cmd) && (echo $limits | jq '.' >&2)
  local limit=$(jq -r '.result[] | select(.name == env.limit_name)' <<<$limits)

  # If a limit was found, then check if we are within it
  if [ -n "$limit" ]; then

    local limit_max=$(jq -r '.max' <<<$limit)
    local limit_rem=$(jq -r '.remaining' <<<$limit)

    if [[ (-z "$limit_rem") || ($limit_rem == null) || ($limit_rem -le 0) ]]; then
      echo "ERROR Max of $limit_max reached for limit $limit_name" >&2
      exit 1
    else
      echo "$limit_rem of $limit_max remaining for limit $limit_name" >&2
    fi

  else
    echo "No limits found for name $limit_name" >&2
  fi
}

create_scratch_org() {
  local devhub=$1
  export local orgname=$2

  # Create the scratch org
  local cmd="sf org create scratch --target-dev-hub $devhub --wait 10 --duration-days 30 --definition-file config/project-scratch-def.json --name $orgname --json" && (echo $cmd >&2)
  local output=$($cmd) && (echo $output | jq '.' >&2)
  scratch_org_username="$(jq -r '.result.username' <<<$output)"
  echo $scratch_org_username >SCRATCH_ORG_USERNAME.txt

  # Get the auth URL
  local cmd="sf org display --verbose --target-org $org_username --json" && (echo $cmd >&2)
  local output=$($cmd)
  org_auth_url="$(jq -r '.result.sfdxAuthUrl' <<<$output)"
  echo $org_auth_url >SCRATCH_ORG_AUTH_URL.txt

  echo $scratch_org_username
}

get_org_auth_url() {

  local org_username=$1
  echo "org_username=$org_username" >&2

  # Parse the SFDX Auth URL for the given org
  local cmd="sf org display --verbose --target-org $org_username --json" && (echo $cmd >&2)
  local output=$($cmd)
  org_auth_url="$(jq -r '.result.sfdxAuthUrl' <<<$output)"

  if [ ! $org_auth_url ]; then
    echo "ERROR No SFDX Auth URL available for org $org_username" >&2
    exit 1
  fi

  # Return the SFDX Auth URL
  echo $org_auth_url
}

push_to_scratch_org() {

  local scratch_org_username=$1

  if [ ! $scratch_org_username ]; then
    echo "ERROR No scratch org username provided to 'push_to_scratch_org' function" >&2
    exit 1
  fi

  # Create a default package.json if file doesn't exist
  if [ ! -f "package.json" ]; then
    npm init -y
  fi

  # Check if the scripts property in package.json contains key for "scratch:deploy"
  cat package.json >&2
  local scriptValue=$(jq -r '.scripts["scratch:deploy"]' <package.json)

  # If no "scratch:deploy" script property, then add one
  if [[ -z "$scriptValue" || $scriptValue == null ]]; then
    local tmp=$(mktemp)
    jq '.scripts["scratch:deploy"]="sf project deploy start"' package.json >$tmp
    mv $tmp package.json
    echo "added scratch:deploy script property to package.json" >&2
    cat package.json >&2
  fi

  # Set the default username so any CLI commands
  # the developer has set in their "test:apex" script in package.json
  # will operate on the correct environment.
  # Afterwards, restore the original default username, just in case it was different.
  local old_org_username=$(jq -r '.result[].value' <<<$(sf config get target-org --json))
  sf config set target-org=$scratch_org_username
  npm run scratch:deploy
  sf config set target-org=$old_org_username

}

populate_scratch_org_redirect_html() {

  local org_username=$1

  if [ ! $org_username ]; then
    echo "ERROR No org username provided to 'populate_scratch_org_redirect_html' function" >&2
    exit 1
  fi

  local cmd="sf org open --target-org $org_username --urlonly --json" && (echo $cmd >&2)
  local output=$($cmd) # don't echo/expose the output which contains the auth url
  local url=$(jq -r ".result.url" <<<$output)

  local environment_html="<script>window.onload=function(){window.location.href=\"$url\"}</script>"
  echo "$environment_html" >ENVIRONMENT.html
  echo "To browse the scratch org, click 'Browse' under 'Job artifacts' and select 'ENVIRONMENT.html'"
}

deploy_scratch_org() {
  local devhub_alias=$1
  local orgname=$2
  assert_within_limits $devhub_alias DailyScratchOrgs
  local scratch_org_username=$(create_scratch_org $devhub_alias $orgname)
  echo $scratch_org_username >SCRATCH_ORG_USERNAME.txt
  get_org_auth_url $scratch_org_username >SCRATCH_ORG_AUTH_URL.txt
  push_to_scratch_org $scratch_org_username
  populate_scratch_org_redirect_html $scratch_org_username
  echo "Deployed to scratch org $username for $orgname"
}

deploy_scratch_org $1 $2
