#!/bin/bash

authenticate() {

  local alias_to_set=$1
  local org_auth_url=$2

  local file=$(mktemp)
  echo $org_auth_url >$file

  local cmd="sf org login sfdx-url --sfdx-url-file $file --alias $alias_to_set --json" && (echo $cmd >&2)

  local output=$($cmd)

  sf config set target-org=$alias_to_set
  sf config set target-org=$alias_to_set

  rm $file
}

authenticate $1 $2
