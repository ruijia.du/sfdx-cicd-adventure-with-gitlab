#!/bin/bash

get_scratch_org_names() {
  local devhub_alias=$1
  local orgname=$2
  local result=$(sf data query --target-org $devhub_alias --query "SELECT SignupUsername FROM ScratchOrgInfo WHERE OrgName='$orgname'" --json)

  apt update && apt -y install jq

  local usernames=$(jq -r ".result.records|map(.SignupUsername)|.[]" <<<$result)
  echo $usernames
}

delete_scratch_orgs() {
  local devhub_alias=$1
  local scratch_org_name=$2
  local orgnames=$(get_scratch_org_usernames $devhub_alias $scratch_org_name)
  for scratch_org_username in $orgnames; do
    echo "Deleting $scratch_org_username"
    local cmd="sf data delete record --sobject ScratchOrgInfo --target $devhub_alias --where "'"SignupUsername='$scratch_org_username'"'" --json" && (echo $cmd >&2)
    local output=$($cmd) && (echo $output | jq '.' >&2)
  done
}

delete_scratch_orgs $1 $2
