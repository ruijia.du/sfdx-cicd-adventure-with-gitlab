#!/bin/bash

test_scratch_org() {

    local org_username=$1

    if [ ! $org_username ]; then
        echo "ERROR No org username provided to 'test_scratch_org' function" >&2
        exit 1
    fi

    # Create a default package.json if file doesn't exist
    if [ ! -f "package.json" ]; then
        npm init -y
    fi

    # Make directory to output test results
    # https://gitlab.com/help/ci/yaml/README.md#artifactsreports
    mkdir -p ./tests/apex

    # Check if the scripts property in package.json contains key for "test:scratch"
    local scriptValue=$(jq -r '.scripts["test:scratch"]' <package.json)

    # If no "test:scratch" script property, then add one
    if [[ -z "$scriptValue" || $scriptValue == null ]]; then
        local tmp=$(mktemp)
        jq '.scripts["test:scratch"]="sfdx force:apex:test:run --codecoverage --resultformat junit --wait 10 --outputdir ./tests/apex"' package.json >$tmp
        mv $tmp package.json
        echo "added test:scratch script property to package.json" >&2
        cat package.json >&2
    fi

    # Set the default username so any CLI commands
    # the developer has set in their "test:scratch" script in package.json
    # will operate on the correct environment.
    # Afterwards, restore the original default username, just in case it was different.
    local old_org_username=$(jq -r '.result[].value' <<<$(sf config get target-org --json))
    sf config set target-org=$org_username
    npm run test:scratch
    sf config set target-org=$old_org_username

}

test_scratch_org $1
