#!/bin/bash

deployMetadata() {

  local org_alias=$1

  if [ ! $org_alias ]; then
    echo "ERROR No org username provided to 'deploy' function" >&2
    exit 1
  fi

  installJq

  if grep -q "<types>" ./manifest/package/package.xml; then
    echo "Local metadata changes detected! Running sf project deploy..."
    local cmd="sf project deploy start -o $org_alias -x manifest/package/package.xml --post-destructive-changes manifest/destructiveChanges/destructiveChanges.xml -l RunLocalTests --wait 10 --json" && (echo $cmd >&2)
    local output=$($cmd) && (echo $output | jq '.' >&2)
    echo $output

    local deployment_status=$(echo $output | jq '.status')

    if [ $deployment_status = 1 ]; then
      echo "ERROR: Deployment to $org_alias failed, please check log output or deployment status for detailed information." >&2
      exit 1
    fi

    echo "Deployment to $org_alias succeeded"

  else
    echo "No local metadata changes to deploy. Please check ./manifest/package/package.xml"
  fi
}

deploy() {
  deployMetadata $1
}

deploy $1
