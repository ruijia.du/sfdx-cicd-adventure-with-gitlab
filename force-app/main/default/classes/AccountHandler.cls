public with sharing class AccountHandler {
  public static void ChangeAccountLevelToSilver(Account acc) {
    acc.Level__c = 'Silver';
  }

  public static void ChangeAccountLevelToDiamond(Account acc) {
    acc.Level__c = 'Diamond';
  }
}
