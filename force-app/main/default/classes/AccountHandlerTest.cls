@isTest
public with sharing class AccountHandlerTest {
  @TestSetup
  static void makeData() {
    Account newAcc = new Account(Name = 'new acc');
    insert newAcc;
  }

  @isTest
  static void ChangeAccountLevelToSilverTest() {
    //given
    Account acc = [
      SELECT id, Name, Level__c
      FROM account
      WHERE name = 'new acc'
    ];

    //when
    System.assertEquals('Bronze', acc.Level__c);
    AccountHandler.ChangeAccountLevelToSilver(acc);

    //then
    System.assertEquals('Silver', acc.Level__c);
  }

  @isTest
  static void ChangeAccountLevelToDiamondTest() {
    //given
    Account acc = [
      SELECT id, Name, Level__c
      FROM account
      WHERE name = 'new acc'
    ];

    //when
    System.assertEquals('Bronze', acc.Level__c);
    AccountHandler.ChangeAccountLevelToDiamond(acc);

    //then
    System.assertEquals('Diamond', acc.Level__c);
  }
}
