# About Project

This is a Salesforce DX Project CI/CD Demo.

`main` branch: Org-based Development CI/CD pipeline example

`package` branch: Package-based Development CI/CD pipeline example

[Click here to sign up Developer Edition Org](https://developer.salesforce.com/signup)

## Issues to be resolved

- Better error handling
- Should add a job for deleting Scratch Org created during pipeline running
- Can't update package version number automatically
- Pipeline should also be triggered when publishing new branch, not only when pushing commits to existing branches
- Should change all sfdx commands to sf commands

# Build A CI/CD Pipeline Step By Step

## Step 0: Design Pipeline Workflow

### Org-based

In this project, we have designed two pipeline workflows for the main(org) branch and org-based-feature branch:

![Org-based-CICD-pipeline-design](./images/Org-based-CICD-pipeline-design.png)

**In this project, We choose scratch org as Test Org and Developer Org as Staging & Prod Org. Because Developer Edition Org can not have sandbox :(**

- **Org-based-feature Branch Pipeline**: [This org-based-feature branch pipeline](https://gitlab.com/the42ndJoanna/sfdx-cicd-adventure-with-gitlab/-/pipelines/932664662) is triggered when changes are committed to the feature branch. It is designed for scratch org review and verifying if this branch is healthy.

- **Main(Org) Branch Pipeline**: [This main branch pipeline](https://gitlab.com/the42ndJoanna/sfdx-cicd-adventure-with-gitlab/-/pipelines/932665460) is triggered when the org-based-feature branch is merged into the main branch. It is designed for continuous integration, pushing changes to the staging and production orgs.

### Package-based

In this project, we have designed two pipeline workflows for the package branch and package-based-feature branch:

![Package-based-CICD-pipeline-design](./images/Package-based-CICD-pipeline-design.png)

**From the above figure, it can be seen that the pipeline of package-based development only has an additional step of creating a package version compared to org-based development :)**

**In this project, We choose scratch org as Test Org and Developer Org as Staging & Prod Org. Because Developer Edition Org can not have sandbox :(**

- **Package-based-feature Branch Pipeline**: [This package-based-feature branch pipeline](https://gitlab.com/the42ndJoanna/sfdx-cicd-adventure-with-gitlab/-/pipelines/932651270) is triggered when changes are committed to the feature branch. It is designed for scratch org review and verifying if this branch is healthy.

- **Package Branch Pipeline**: [This package branch pipeline](https://gitlab.com/the42ndJoanna/sfdx-cicd-adventure-with-gitlab/-/pipelines/932651961) is triggered when the package-based-feature branch is merged into the package branch. It is designed for continuous integration, pushing changes to the staging and production orgs.

## Step 1: Set Environment Variables for Org Authentication & Other usages

In Step 0, we defined pipeline workflows that requires three environments(Strictly speaking, the Dev Org is not within the scope of the automated pipeline. Developers manually create a Dev Org and interact with it manually during development):

- Dev Hub(for creating Scratch Org, and you need to enable Dev Hub feature first to make an org become a Dev Hub, see: [How to enable Dev Hub feature](https://help.salesforce.com/s/articleView?id=sf.sfdx_setup_enable_devhub.htm&type=5))
- Staging
- Production

And they are all orgs. Salesforce requires authentication before we can interact with these environments.

### Wait, let's talk about Org Authentication first

For your information, there are two different methods to authenticate:

> **Note:** The following content is just an introduction to two different authentication methods. If you are building your own project, you can choose either one. In this project, we use **sfdxurl command** and you can refer to the `authenticate.sh` script in `cicd-scripts` directory.

#### 1. jwt Commands

We can authenticate using jwt commands with the following command:

```shell
# using sfdx
sfdx auth:jwt:grant --instanceurl $ENDPOINT --clientid $DEVHUB_CONSUMER_KEY --jwtkeyfile assets/server.key.enc --username $DEVHUB_USER_NAME --setalias DEVHUB
# or using sf
sf org login jwt --username $ORG_USERNAME --jwt-key-file assets/server.key.enc --client-id $CONSUMER_KEY --alias $ORG_ALIAS --instance-url $ENDPOINT
```

The value of `ENDPOINT` is usually `https://login.salesforce.com/` for orgs or `https://test.salesforce.com` for sandboxes, or your custom subdomain.

> **How to obtain consumer key:** In your Dev Hub org, [create a connected app](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_connected_app.htm) as described by the JWT-based authorization flow. This step includes obtaining or [creating a private key and digital certificate](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_key_and_cert.htm).

> **How to encrypt and decrypt your server.key:** Please refer to the sixth step in [this document](https://github.com/forcedotcom/sfdx-gitlab-org).

For more info on setting up JWT-based auth, see [Authorize an Org Using the JWT-Based Flow](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_jwt_flow.htm?q=consumer%20key) in the [Salesforce DX Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_intro.htm).

#### 2. sfdxurl Commands(Used in this project)

We can authenticate using sfdx commands with the following command:

```shell
# using sfdx
sfdx force:auth:sfdxurl:store --sfdxurlfile $file --setalias $alias_to_set --json
# org using sf
sf org login sfdx-url --sfdx-url-file $file --alias $ORG_ALIAS --json
```

See more about [sfdxurl Commands](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference_auth_sfdxurl.htm).

As we are using sfdxurl Commands in the project, we need to obtain the Auth URL for DEV HUB, Staging, and Production.

> **Note:** To get the values for your auth URL's, make sure you're logged in with your `sf cli` locally and run the following command: `sf org display -o $ORG_USERNAME_OR_ALIAS --verbose --json`

### Now, real Environment Variables for you

**What are the uses of environment variables? Just like many other variables, you can refer to these variables multiple times in a YAML file using the "$" symbol.**
By using environment variables, you can avoid duplicating the definition of the same values across multiple configuration files, and you can easily modify these values as needed.

Here are some environment variable examples that need to be set to execute the pipelines in this project:

| **Variable**                     | **Value**                                                                                                                                                                                |
| -------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `DEPLOY_SCRATCH_ON_EVERY_COMMIT` | `true`                                                                                                                                                                                   |
| `ORG_BASED_DEVHUB_AUTH_URL`      | `force://PlatformCLI::5Aep861j70kdjF1ZXM.....RETRACTED....LtDgJ0hF5MK@trailhead-test-automation-dev-ed.my.salesforce.com`                                                                |
| `ORG_BASED_PROD_AUTH_URL`        | `force://PlatformCLI::5Aep861dR8ARTEERo7e5oXTq1LCEck8Ilv5f1ITys76XJc24c1r7sp2mhDznizylcMffOmkNv3Ntb.....RETRACTED....v29mDKuFo5@nosoftware-momentum-6427-dev-ed.cs40.my.salesforce.com/` |
| `ORG_BASED_STAGING_AUTH_URL`     | `force://PlatformCLI::5Aep861ulCxXNoWflpm9.....RETRACTED...._ZKwhkYGo2cQG@inspiration-dream-8897-dev-ed.cs41.my.salesforce.com/`                                                         |

> **Note:** Environment variables in GitLab are configured by your administrator to be protected by default. Protected variables are only exposed to protected branches or protected tags. If you want certain environment variables to be available for non-protected branches, please uncheck their "protected" flag.

Here are those real environment variables used in this project:

![environment-variables](./images/environment-variables.png)

## Step 1.1: Create Package Manually(only for Package-based Development)

Yes, our pipeline can create package **version** automatically. But before that, we need to create a package in Dev Hub manually. It's very easy and you **only need to do it once**. Use the following command:

```shell
sf package create --name <YourPackageName> --package-type Unlocked --path <YourPackageDirectory, usually force-app> --target-dev-hub <Dev Hub username or alias>
```

If you want to know why this should be done, see: [Workflow for Unlocked Packages
](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_unlocked_pkg_workflow.htm)

## Step 2: YAML & Scripts

In this step, we'll simply introduce `.gitlab-ci.yml` in our project. To learn how to write GitLab YAML files, please refer to [The .gitlab-ci.yml file](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)..

### Org-based

We have defined four stages in our YAML configuration: `create-scratch-org`, `test-scratch-org`, `staging`, and `production`. Typically, changes made to the feature branch will only trigger the `create-scratch-org` and `test-scratch-org` stages, while changes to the main branch will trigger all stages.

```yml
stages:
  - create-scratch-org
  - test-scratch-org
  - staging
  - production
```

Here is the configuration for `create-scratch-org` job. This stage will only be triggered in the pipeline and execute the scripts if new changes are made to main branch and `DEPLOY_SCRATCH_ON_EVERY_COMMIT` is set to true.

```yml
create-scratch-org:
  stage: create-scratch-org
  only:
    variables:
      - $DEPLOY_SCRATCH_ON_EVERY_COMMIT == 'true'
  allow_failure: false
  script:
    - bash cicd-scripts/install_salesforce_cli.sh
    - bash cicd-scripts/install_jq.sh
    - bash cicd-scripts/authenticate.sh DEVHUB $ORG_BASED_DEVHUB_AUTH_URL $ORG_BASED_PRODUCTION_AUTH_URL
    - bash cicd-scripts/delete_scratch_orgs.sh DEVHUB "scratch/$CI_COMMIT_REF_SLUG"
    - bash cicd-scripts/deploy_scratch_org.sh DEVHUB "scratch/$CI_COMMIT_REF_SLUG"
  environment:
    name: scratch/$CI_COMMIT_REF_SLUG
    url: $CI_PROJECT_URL/-/jobs/$CI_JOB_ID/artifacts/file/ENVIRONMENT.html
  artifacts:
    paths:
      - ENVIRONMENT.html
      - SCRATCH_ORG_USERNAME.txt
      - SCRATCH_ORG_AUTH_URL.txt
```

### Package-based

For package-based development pipeline, there is an additional stage for creating package version.

```yml
stages:
  - create-scratch-org
  - test-scratch-org
  - create-package-version
  - staging
  - production
```

Here is the configuration for `create-package-version` job.

```yml
create-package-version:
  stage: create-package-version
  only:
    variables:
      - $CI_COMMIT_REF_NAME == 'package'
  allow_failure: false
  script:
    - bash cicd-scripts/install_salesforce_cli.sh
    - bash cicd-scripts/install_jq.sh
    - bash cicd-scripts/authenticate.sh DEVHUB $PKG_BASED_DEVHUB_AUTH_URL $PKG_BASED_PRODUCTION_AUTH_URL
    - bash cicd-scripts/assert_within_limits.sh DEVHUB Package2VersionCreates
    - package_id=$(bash cicd-scripts/get_package_id.sh DEVHUB $PACKAGE_NAME)
    - echo $package_id > PACKAGE_ID.txt
    - bash cicd-scripts/add_package_alias.sh DEVHUB $package_id
    - package_version_id=$(bash cicd-scripts/build_package_version.sh DEVHUB $package_id)
    - echo $package_version_id > SUBSCRIBER_PACKAGE_VERSION_ID.txt
  artifacts:
    paths:
      - PACKAGE_ID.txt
      - SUBSCRIBER_PACKAGE_VERSION_ID.txt
```

> Find more info in .gitlab-ci.yml file! **Don't forget to switch your branch to `package` if you want to see yaml file of package-based pipeline.**

## Step 4: Branching, Committing, Merging, and Observing the Pipeline

With our YAML configuration set up, we can now start creating branches, pushing commits, merging branches, and observing the pipeline. Good luck!

### How to open Scratch Org & other artifacts created during pipeline running

Choose your button and click:

![job-artifacts](./images/job-artifacts.png)

## Troubleshooting

### Org Limits

The number of ActiveScratchOrg and DailyScratchOrg that each Org can create is limited. If the limit is reached, the pipeline will throw an error. Please refer to the [Supported Scratch Org Allocation](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_scratch_orgs_editions_and_allocations.htm) for more information.

In this project, we used the script `assert_within_limits` to output the limits of the current Dev Hub org in the log.

```bash
  function assert_within_limits() {

    export local org_username=$1
    export local limit_name=$2
    echo "org_username=$org_username" >&2
    echo "limit_name=$limit_name" >&2

    local cmd="sfdx force:limits:api:display --targetusername $org_username --json" && (echo $cmd >&2)
    local limits=$($cmd) && (echo $limits | jq '.' >&2)
    local limit=$(jq -r '.result[] | select(.name == env.limit_name)' <<< $limits)

    # If a limit was found, then check if we are within it
    if [ -n "$limit" ]; then

      local limit_max=$(jq -r '.max' <<< $limit)
      local limit_rem=$(jq -r '.remaining' <<< $limit)

      if [[ ( -z "$limit_rem" ) || ( $limit_rem == null ) || ( $limit_rem -le 0 ) ]]; then
        echo "ERROR Max of $limit_max reached for limit $limit_name" >&2
        exit 1
      else
        echo "$limit_rem of $limit_max remaining for limit $limit_name" >&2
      fi

    else
      echo "No limits found for name $limit_name" >&2
    fi
  }
```

#### Log Output Example

![devhub-org-limits](./images/devhub-org-limits.png)

# Feel Free to Contact Me Via Email/Google Chat/WeChat If You Have Any Advice Or Questions :)
